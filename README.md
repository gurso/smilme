# SmilMe

## TODO

### fix/priority

- what happen when remove position attributes (ex: from AttrsForm)
- ux locked and fix svg/g behavior...
- ui feed back for locked shapes
- history used bytes do not decrease when save after undo

### features

- TransformCtrl component/mode
  - replace transform rotate by real position (path, poly, line)
- begin attr : https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/begin
- clipPath / view / filter / marker / symbol / pattern
- radialGradientCreator / handle transparent / handle string with %
- all fe\*\*\*
- class handler (CSS className !)
- meta data with data-smilme ?
- toPath method (Text 😱)
  - merge path !

#### keyboard

- modal to docs shortcut
- copy / paste with keyboard
- ctrl + g
- export svg on ctrl+ s || ctrl + e (what about name ?)

#### text

- textPath: https://developer.mozilla.org/en-US/docs/Web/SVG/Element/textPath
- motion
- scale
- tspan and text breakLine

### refact

- history (save all Tags, actually only on shapes) (use vueUse ?)
- organize types
- better type for Attrs ? props type as number | ${number} ?

#### tree folder

```
app/
components/
    ctrls/
        preload/
        commands/
    forms/
        preload/
```
