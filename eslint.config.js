import { includeIgnoreFile } from "@eslint/compat"
import path from "node:path"
import { fileURLToPath } from "node:url"

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const gitignorePath = path.resolve(__dirname, ".gitignore")

import conf from "@gurso/eslint-config"
export default [
	includeIgnoreFile(gitignorePath),
	...[
		...conf,
		{
			files: ["*.vue", "**/*.vue"],
			languageOptions: {
				parserOptions: {
					parser: "@typescript-eslint/parser",
				},
			},
			rules: {
				"vue/no-mutating-props": ["error", { shallowOnly: true }],
			},
		},
	],
]
