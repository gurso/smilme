import { mount } from "@vue/test-utils"
import { describe, expect, test } from "vitest"
import { Component } from "vue"

const icons = import.meta.glob<{ default: Component }>("./*Icon.vue", { eager: true })
const arr = Object.entries(icons).map(([name, icon]) => ({ name, icon }))

const props = {
	color: "red",
	size: 42,
}

describe.each(arr)("Test all icons", ({ name, icon }) => {
	const wrapper = mount(icon.default, { props })

	test("Test icon : " + name, () => {
		const tempWrapper = wrapper.find("svg")
		expect(tempWrapper.exists()).toBe(true)
		const width = tempWrapper.attributes("width")
		const height = tempWrapper.attributes("height")
		expect(width).toBe(props.size.toString())
		expect(height).toBe(props.size.toString())
	})
})
