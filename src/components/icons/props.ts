import { PropType } from "vue"

export const propsIcon = {
	color: {
		type: String,
		default: "currentColor",
	},
	size: {
		type: [Number, String] as PropType<number | `${number}`>,
		default: 24,
	},
}
