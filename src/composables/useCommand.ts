import type { IsCommand } from "../types/IsCommand"
import { MaybeRef, computed, ref } from "vue"
import { Path } from "../models"
import { CommandVertical } from "../models/CommandVertical"
import { CommandHorizontal } from "../models/CommandHorizontal"
import { CommandZ } from "../models/CommandZ"
import { command as selectedCommand } from "../store/selected"
import { onMouseMoveUntilUp } from "../mods/dom"
import { useRatio } from "./useViewbox"
import { useZoom } from "./useZoom"
import { useCursor } from "./useCursor"

export function useCommand(command: MaybeRef<IsCommand | undefined>, path: Path, offset = { x: 0, y: 0 }) {
	const { zoomed: zoomedCursorCoord } = useCursor(offset)
	const refCommand = ref(command)
	const isSelected = computed(() => selectedCommand.value?.id === refCommand.value?.id)
	const isAbsolute = computed(() => refCommand.value?.isAbs())
	const cx = computed(() => path.getAbsCoord("x", refCommand.value))
	const cy = computed(() => path.getAbsCoord("y", refCommand.value))

	const { x: v } = useRatio(2)
	const { unZoomed: strokeWidth } = useZoom(v)

	function onCommandMouseDown(c: Exclude<IsCommand, CommandZ>) {
		c.mousedown(zoomedCursorCoord.value)
		selectedCommand.value = c
		onMouseMoveUntilUp(() => {
			if (c.isAbs()) {
				c.move(zoomedCursorCoord.value)
			} else {
				if (c instanceof CommandVertical) {
					const y = c.y + (zoomedCursorCoord.value.y - cy.value) - offset.y
					c.move({ y })
				} else if (c instanceof CommandHorizontal) {
					const x = c.x + (zoomedCursorCoord.value.x - cx.value) - offset.x
					c.move({ x })
				} else {
					const y = c.y + (zoomedCursorCoord.value.y - (cy.value + offset.y))
					const x = c.x + (zoomedCursorCoord.value.x - (cx.value + offset.x))
					c.move({ x, y })
				}
			}
		})
	}

	return {
		isSelected,
		isAbsolute,
		cx,
		cy,
		onCommandMouseDown,
		strokeWidth,
	}
}
