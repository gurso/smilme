import { computed, MaybeRef, ref } from "vue"
import { Coord } from "../types/Shape"
import { svg, x as _x, y as _y } from "../store/canvas"
import { zoom } from "../store/zoom"
import { useSvg } from "./useSvg"

export function useCursor(offset: MaybeRef<Coord> = { x: 0, y: 0 }) {
	const _offset = ref(offset)
	const { viewBox } = useSvg(svg)
	const zoomed = computed(() => {
		const x = (_x.value / zoom.value / svg.value.attrs.width) * (viewBox.value.width - viewBox.value.x)
		const y = (_y.value / zoom.value / svg.value.attrs.height) * (viewBox.value.height - viewBox.value.y)
		return { x, y }
	})

	const translated = computed(() => {
		const x = zoomed.value.x + _offset.value.x
		const y = zoomed.value.y + _offset.value.y
		return { x, y }
	})

	return { zoomed, translated }
}
