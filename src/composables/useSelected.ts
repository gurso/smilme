import { computed } from "vue"
import { HasChildren } from "../models"
import { G } from "../models/G"
import { select, selected } from "../store/selected"
import { svg } from "../store/canvas"

export function useSelected() {
	const getLowestCommonAncestor = (a: HasChildren): HasChildren => {
		const lower = a.children.find(
			(child): child is HasChildren => child instanceof HasChildren && child.isAncestorOf(...selected.value),
		)
		return lower ? getLowestCommonAncestor(lower) : a
	}

	const commonAncestor = computed(() => (selected.value.length ? getLowestCommonAncestor(svg.value) : svg.value))

	function _group(parent: HasChildren) {
		if (parent.isParentOf(...selected.value)) {
			parent.rm(...selected.value)
			const g = new G(selected.value)
			parent.children.push(g)
			select(g)
			return true
		} else if (parent.isAncestorOf(...selected.value)) {
			for (const child of parent.children) {
				if (child instanceof HasChildren) {
					if (_group(child)) return true
				}
			}
		}
	}

	function group() {
		_group(commonAncestor.value)
	}

	return { commonAncestor, group }
}
