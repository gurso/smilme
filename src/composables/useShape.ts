import { MaybeRef, computed, ref, watch, onMounted, onUnmounted } from "vue"
import { Shape } from "../models"
import { Coord } from "../types/Shape"
import { onMouseMoveUntilUp } from "../mods/dom"
import { select, toggle, selected, isSelected as isSel } from "../store/selected"
import { G } from "../models/G"
import { saveDebounced } from "../store/history"
import { useCursor } from "./useCursor"

export function useShape(_shape: MaybeRef<Shape | G>) {
	const shape = ref(_shape)
	const { zoomed: zoomedCursorCoord } = useCursor()
	const onShapeMouseDown = () => {
		if (!shape.value.lock) {
			if (!isSel(shape.value)) select(shape.value)
			for (const s of selected.value) {
				if (s instanceof Shape || s instanceof G) {
					s.mousedown(zoomedCursorCoord.value)
				}
			}
			onMouseMoveUntilUp(() => {
				for (const s of selected.value) {
					if (s instanceof Shape || s instanceof G) {
						s.move(zoomedCursorCoord.value)
					}
				}
			})
		}
	}

	function onMouseDownCtrl() {
		toggle(shape.value)
	}

	const isSelected = computed(() => isSel(shape.value))

	function onScale({ x, y }: Coord, mode?: "min" | "max") {
		if (mode === "min") x = y = Math.min(x, y)
		else if (mode === "max") x = y = Math.max(x, y)
		shape.value.scale({ x, y })
	}

	const viewBox = computed(() => shape.value.getViewBox())

	watch(shape, saveDebounced, { deep: true })

	onMounted(saveDebounced)
	onUnmounted(saveDebounced)

	return { onShapeMouseDown, onMouseDownCtrl, onScale, viewBox, isSelected }
}
