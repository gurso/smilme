import { Ref, computed } from "vue"
import { Svg } from "../models/Svg"
import { toTuple } from "../mods/tuple"
import { selected } from "../store/selected"
import { RectBox } from "../types/Shape"

export function useSvg(svg: Ref<Svg>) {
	const viewBox = computed(() => {
		const [x, y, width, height] = toTuple(svg.value.attrs.viewBox.split(" ").map(Number) ?? [], 4)
		return { x, y, width, height }
	})

	function setViewBox(values: Partial<RectBox>) {
		const { x, y, width, height } = { ...viewBox.value, ...values }
		svg.value.attrs.viewBox = `${x} ${y} ${width} ${height}`
	}

	const isSelected = computed(() => selected.value.includes(svg.value))

	const viewBoxIsValid = computed(
		() =>
			svg.value.attrs.width / (viewBox.value.width - viewBox.value.x) ===
			svg.value.attrs.height / (viewBox.value.height - viewBox.value.y),
	)

	return { viewBox, viewBoxIsValid, isSelected, setViewBox }
}
