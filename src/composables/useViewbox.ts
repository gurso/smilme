import { MaybeRef, computed, ref } from "vue"
import { svg } from "../store/canvas"
import { useSvg } from "./useSvg"

export function useRatio(_px: MaybeRef<number>) {
	const px = ref(_px)
	const { viewBox } = useSvg(svg)
	const x = computed(() => (px.value / svg.value.attrs.width) * viewBox.value.width)
	const y = computed(() => (px.value / svg.value.attrs.height) * viewBox.value.height)
	return { x, y }
}
