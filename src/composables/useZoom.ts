import { Ref, computed } from "vue"
import { zoom } from "../store/zoom"

export function useZoom(r: Ref<number>) {
	const unZoomed = computed(() => r.value / zoom.value)
	return { unZoomed }
}
