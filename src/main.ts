import { createApp } from "vue"
import "./style.css"
import App from "./App.vue"

import CircleShape from "./components/CircleShape.vue"
import RectShape from "./components/RectShape.vue"
import LineShape from "./components/LineShape.vue"
import EllipseShape from "./components/EllipseShape.vue"
import PathShape from "./components/PathShape.vue"
import GShape from "./components/GShape.vue"
import PolyShape from "./components/PolyShape.vue"
import TextShape from "./components/TextShape.vue"
import DefsShape from "./components/DefsShape.vue"
import MaskShape from "./components/MaskShape.vue"
import UseShape from "./components/UseShape.vue"
import GradientShape from "./components/GradientShape.vue"
import StyleSheetShape from "./components/StyleSheetShape.vue"
import ImageShape from "./components/ImageShape.vue"
import TagShape from "./components/TagShape.vue"
import HasChildrenShape from "./components/HasChildrenShape.vue"

import CircleForm from "./components/CircleForm.vue"
import RectForm from "./components/RectForm.vue"
import LineForm from "./components/LineForm.vue"
import EllipseForm from "./components/EllipseForm.vue"
import PathForm from "./components/PathForm.vue"
import GForm from "./components/GForm.vue"
import PolyForm from "./components/PolyForm.vue"
import TextForm from "./components/TextForm.vue"
import DefsForm from "./components/DefsForm.vue"
import MaskForm from "./components/MaskForm.vue"
import UseForm from "./components/UseForm.vue"
import GradientForm from "./components/GradientForm.vue"
import StyleSheetForm from "./components/StyleSheetForm.vue"
import ImageForm from "./components/ImageForm.vue"
import SvgForm from "./components/SvgForm.vue"
import HasChildrenForm from "./components/HasChildrenForm.vue"
import TagForm from "./components/TagForm.vue"

import CommandForm from "./components/CommandForm.vue"
import PointForm from "./components/PointForm.vue"

const app = createApp(App)

app.component("CircleShape", CircleShape)
app.component("RectShape", RectShape)
app.component("EllipseShape", EllipseShape)
app.component("LineShape", LineShape)
app.component("PathShape", PathShape)
app.component("GShape", GShape)
app.component("PolyShape", PolyShape)
app.component("TextShape", TextShape)
app.component("MaskShape", MaskShape)
app.component("DefsShape", DefsShape)
app.component("UseShape", UseShape)
app.component("GradientShape", GradientShape)
app.component("StyleSheetShape", StyleSheetShape)
app.component("ImageShape", ImageShape)
app.component("HasChildrenShape", HasChildrenShape)
app.component("TagShape", TagShape)

app.component("SvgForm", SvgForm)
app.component("CircleForm", CircleForm)
app.component("RectForm", RectForm)
app.component("LineForm", LineForm)
app.component("EllipseForm", EllipseForm)
app.component("PathForm", PathForm)
app.component("CommandForm", CommandForm)
app.component("GForm", GForm)
app.component("PolyForm", PolyForm)
app.component("PointForm", PointForm)
app.component("TextForm", TextForm)
app.component("MaskForm", MaskForm)
app.component("DefsForm", DefsForm)
app.component("UseForm", UseForm)
app.component("GradientForm", GradientForm)
app.component("StyleSheetForm", StyleSheetForm)
app.component("ImageForm", ImageForm)
app.component("TagForm", TagForm)
app.component("HasChildrenForm", HasChildrenForm)

app.mount("#app")
