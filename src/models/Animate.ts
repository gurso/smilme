import { AnimateAttrs, Value } from "../types/Animation"
import { parse, serialize } from "../mods/attrs"
import { Tag } from "./Tag"

export class Animate extends Tag {
	static fromDom(el: SVGAnimationElement) {
		const attributeName = el.getAttribute("attributeName")
		if (!attributeName) throw new Error("attributeName not found")
		const values = el.getAttribute("values")
		const animate = new Animate(attributeName)
		const attrs = parse(el)
		animate.setAttrs(attrs)
		if (values) animate.values = values.split(";")
		return animate
	}

	readonly uuid = crypto.randomUUID()
	isActive = true

	attrs: AnimateAttrs & { attributeName: string }
	values: Value[] = []

	constructor(attributeName: string) {
		super(attributeName === "transform" ? "animateTransform" : "animate")
		this.attrs = { attributeName }
	}

	toString() {
		const values = this.values.length ? `values="${this.values.join(";")}"` : ""
		return `<${this.html} ${values} ${serialize(this.attrs)}></${this.html}>`
	}
}
