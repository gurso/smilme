import { parsePathD, serializePathD } from "../mods/path"
import { parse, serialize } from "../mods/attrs"
import { AnimateAttrs } from "../types/Animation"
import { Tag } from "./Tag"
import { Path } from "./"

type MotionAttrs = {
	keypoints?: number
	rotate?: "auto" | "auto-reverse" | number
	repeatCount: number | "indefinite"
}

export class AnimateMotion extends Tag {
	static fromDom(el: SVGAnimationElement) {
		const animate = new AnimateMotion()
		const attrs = parse(el)
		const pathAttr = el.getAttribute("path")
		delete attrs.path
		animate.setAttrs(attrs)
		// TODO el.querySelector("mpath")
		animate.path.commands = parsePathD(pathAttr ?? "")

		return animate
	}

	readonly uuid = crypto.randomUUID()

	attrs: AnimateAttrs & MotionAttrs = { dur: 4, repeatCount: "indefinite" }

	path = new Path()
	// mpath?: Path, string ???

	constructor() {
		super("animateMotion")
		this.path.attrs.fill = "none"
		this.path.attrs.stroke = "gray"
		this.path.attrs["stroke-width"] = "1px"
	}

	toString() {
		return `<animateMotion ${serialize(this.attrs)} path="${serializePathD(this.path.commands)}"></animateMotion>`
	}
}
