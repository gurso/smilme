import { AnimateAttrs, TransformType } from "../types/Animation"
import { Animate } from "./Animate"
import { parse } from "../mods/attrs"

export class AnimateTransform extends Animate {
	static fromDom(el: SVGAnimationElement) {
		const type = el.getAttribute("type")
		if (!type) throw new Error("Missing type attribute on animateTraform element")
		const animate = new AnimateTransform(type as TransformType)
		animate.setAttrs(parse(el))
		return animate
	}

	declare attrs: AnimateAttrs & { attributeName: "transform"; type: TransformType }

	constructor(type: TransformType) {
		super("transform")
		this.setAttrs({ type })
	}
}
