import { Attrs, getNumbers, parse } from "../mods/attrs"
import { Coord } from "../types/Shape"
import { Shape } from "./"
import { Svg } from "./Svg"

type CircleAttrs = {
	cx: number
	cy: number
	r: number
} & Attrs

export class Circle extends Shape {
	// static required = ["cx", "cy", "r"] as const
	constructor() {
		super("circle")
	}

	attrs: CircleAttrs = {
		r: 0,
		cx: 0,
		cy: 0,
	}

	static fromDom(el: SVGCircleElement) {
		const circle = new Circle()
		const attrs = parse(el)
		circle.setAttrs(attrs)
		circle.setAttrs(getNumbers(attrs, ["cx", "cy", "r"]))
		circle.parseAnimation(el)
		return circle
	}

	getViewBox() {
		const x = this.attrs.cx - this.attrs.r
		const y = this.attrs.cy - this.attrs.r
		const width = 2 * this.attrs.r
		const height = 2 * this.attrs.r
		return { x, y, width, height }
	}

	scale({ x, y }: Coord, mode: "min" | "max" = "max") {
		const ratio = mode === "max" ? Math.max(x, y) : Math.min(x, y)
		const minX = this.attrs.cx - this.attrs.r
		const minY = this.attrs.cy - this.attrs.r
		const r = this.attrs.r * ratio
		this.attrs.cx = minX + r
		this.attrs.cy = minY + r
		this.attrs.r = r
	}

	mousedown({ x, y }: Coord) {
		this.offsetX = x - this.attrs.cx
		this.offsetY = y - this.attrs.cy
	}

	move({ x, y }: Coord) {
		this.attrs.cx = x - this.offsetX
		this.attrs.cy = y - this.offsetY
	}

	round(dec?: number) {
		this.attrs.r = Number(this.attrs.r.toFixed(dec))
		this.attrs.cx = Number(this.attrs.cx.toFixed(dec))
		this.attrs.cy = Number(this.attrs.cy.toFixed(dec))
		super.round(dec)
	}

	reverseY(svg: Svg) {
		this.attrs.cy = svg.getReverseY(this.attrs.cy)
	}

	reverseX(svg: Svg) {
		this.attrs.cx = svg.getReverseX(this.attrs.cx)
	}
}
