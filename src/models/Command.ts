import { CommandName } from "../mods/commands"
import { IsCommand } from "../types/IsCommand"
import { Ctrl } from "./Ctrl"
import { Coord, RectBox } from "../types/Shape"

export abstract class Command {
	id = Symbol()
	abstract name: CommandName

	abstract round(dec?: number): void
	abstract scale(ratio: Coord, pathViewBox: RectBox): void

	isAbs() {
		return this.name === this.name.toUpperCase()
	}

	clone(this: IsCommand & Partial<Coord>) {
		const c = new (this.constructor as new (coord: Partial<Coord>) => IsCommand & Partial<Coord>)(this)
		Object.assign(c, this)
		c.id = Symbol()
		if ("ctrls" in c) {
			c.ctrls = c.ctrls.map(ctrl => new Ctrl(c, ctrl)) as typeof c.ctrls
		}
		return c
	}
}
