import { Coord, RectBox } from "../types/Shape"
import { Command } from "./Command"
import { parseValues } from "../mods/commands"
import { Svg } from "./Svg"

export class CommandArc extends Command {
	name: "a" | "A" = "A"
	xAxisRotation = 0
	largeArcFlag = false
	sweepFlag = false
	rx = 0
	ry = 0

	x = 0
	y = 0

	offsetX = 0
	offsetY = 0

	static parse(name: "a" | "A", s: string) {
		const values = parseValues(s, true)
		if (values.length % 7 !== 0) console.error("CommandArc parse will probably failed !", s, values)
		const commands: CommandArc[] = []
		while (values.length) {
			const rx = values.shift()!
			const ry = values.shift()!
			const xAxisRotation = values.shift()!
			const largeArcFlag = values.shift()!
			const sweepFlag = values.shift()!
			const x = values.shift()!
			const y = values.shift()!
			const command = new CommandArc({ x, y })
			command.name = name
			Object.assign(command, { rx, ry, xAxisRotation, largeArcFlag, sweepFlag })
			commands.push(command)
		}
		return commands
	}

	constructor({ x, y }: Coord) {
		super()
		this.x = x
		this.y = y
	}

	mousedown({ x, y }: Readonly<Coord>) {
		this.offsetX = this.isAbs() ? x - this.x : 0
		this.offsetY = this.isAbs() ? y - this.y : 0
	}

	move({ x, y }: Readonly<Coord>) {
		this.x = x - this.offsetX
		this.y = y - this.offsetY
	}

	toString() {
		const { xAxisRotation: xar, sweepFlag: sf, largeArcFlag: laf } = this
		return `${this.name} ${this.rx} ${this.ry} ${xar} ${Number(laf)} ${Number(sf)} ${this.x} ${this.y}`
	}

	round(dec?: number) {
		this.rx = Number(this.rx.toFixed(dec))
		this.ry = Number(this.ry.toFixed(dec))
		this.x = Number(this.x.toFixed(dec))
		this.y = Number(this.y.toFixed(dec))
	}

	scale(ratio: Coord, pathViewBox: RectBox) {
		if (this.isAbs()) {
			const relX = this.x - pathViewBox.x
			this.x = pathViewBox.x + relX * ratio.x
			const relY = this.y - pathViewBox.y
			this.y = pathViewBox.y + relY * ratio.y
		} else {
			this.x *= ratio.x
			this.y *= ratio.y
		}
		this.rx *= ratio.x
		this.ry *= ratio.y
	}

	reverseX(svg: Svg) {
		if (this.isAbs()) this.x = svg.getReverseX(this.x)
		else this.x *= -1
		this.sweepFlag = !this.sweepFlag
	}
	reverseY(svg: Svg) {
		if (this.isAbs()) this.y = svg.getReverseY(this.y)
		else this.y *= -1
		this.sweepFlag = !this.sweepFlag
	}
}
