import type { Coord, RectBox } from "../types/Shape"
import type { IsCommand } from "../types/IsCommand"
import { Ctrl } from "./Ctrl"
import { Command } from "./Command"
import { Svg } from "./Svg"
import { parseValues } from "../mods/commands"
import { barryCenter } from "../mods/barryCenter"

export class CommandCubic extends Command {
	name: "c" | "C" = "C"
	ctrls: [Ctrl, Ctrl]

	x = 0
	y = 0

	offsetX = 0
	offsetY = 0

	static parse(name: "c" | "C", s: string) {
		const values = parseValues(s)
		if (values.length % 6 !== 0) {
			console.error("CommandCubic parse will probably failed !", s, values.length)
			// console.table(values)
		}
		const commands: CommandCubic[] = []
		while (values.length) {
			const x1 = values.shift()!
			const y1 = values.shift()!
			const x2 = values.shift()!
			const y2 = values.shift()!
			const x = values.shift()!
			const y = values.shift()!
			const command = new CommandCubic({ x, y })
			command.name = name
			const c1 = new Ctrl(command, { x: x1, y: y1 })
			const c2 = new Ctrl(command, { x: x2, y: y2 })
			command.ctrls = [c1, c2]
			commands.push(command)
		}
		return commands
	}

	constructor({ x, y }: Coord) {
		super()
		this.x = x
		this.y = y
		this.ctrls = [new Ctrl(this), new Ctrl(this)]
	}
	mousedown({ x, y }: Readonly<Coord>) {
		this.offsetX = this.isAbs() ? x - this.x : 0
		this.offsetY = this.isAbs() ? y - this.y : 0
		for (const c of this.ctrls) {
			c.mousedown({ x, y })
		}
	}

	move({ x, y }: Readonly<Coord>) {
		this.x = x - this.offsetX
		this.y = y - this.offsetY
		for (const c of this.ctrls) {
			c.move({ x, y })
		}
	}

	toString() {
		const [a, b] = this.ctrls
		return `${this.name} ${a.x} ${a.y}, ${b.x} ${b.y}, ${this.x} ${this.y}`
	}

	resetCtrls(bfr: IsCommand & Coord) {
		const c1 = barryCenter(bfr, this, 0.66)
		const c2 = barryCenter(bfr, this, 0.33)
		this.ctrls = [new Ctrl(this, c1), new Ctrl(this, c2)]
	}

	round(dec?: number) {
		this.x = Number(this.x.toFixed(dec))
		this.y = Number(this.y.toFixed(dec))
		for (const ctrl of this.ctrls) {
			ctrl.x = Number(ctrl.x.toFixed(dec))
			ctrl.y = Number(ctrl.y.toFixed(dec))
		}
	}

	scale(ratio: Coord, pathViewBox: RectBox) {
		if (this.isAbs()) {
			const relX = this.x - pathViewBox.x
			this.x = pathViewBox.x + relX * ratio.x
			const relY = this.y - pathViewBox.y
			this.y = pathViewBox.y + relY * ratio.y
			for (const ctrl of this.ctrls) {
				const relX = ctrl.x - pathViewBox.x
				ctrl.x = pathViewBox.x + relX * ratio.x
				const relY = ctrl.y - pathViewBox.y
				ctrl.y = pathViewBox.y + relY * ratio.y
			}
		} else {
			this.x *= ratio.x
			this.y *= ratio.y
			for (const ctrl of this.ctrls) {
				ctrl.x *= ratio.x
				ctrl.y *= ratio.y
			}
		}
	}

	reverseX(svg: Svg) {
		if (this.isAbs()) this.x = svg.getReverseX(this.x)
		else this.x *= -1
		for (const ctrl of this.ctrls) {
			ctrl.x = svg.getReverseX(ctrl.x)
		}
	}

	reverseY(svg: Svg) {
		if (this.isAbs()) this.y = svg.getReverseY(this.y)
		else this.y *= -1
		for (const ctrl of this.ctrls) {
			ctrl.y = svg.getReverseY(ctrl.y)
		}
	}
}
