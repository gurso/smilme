import { parseValues } from "../mods/commands"
import { Coord, RectBox } from "../types/Shape"
import { Command } from "./Command"
import { Svg } from "./Svg"

export class CommandHorizontal extends Command {
	name: "h" | "H" = "H"

	x = 0
	offsetX = 0

	constructor({ x }: { x: number }) {
		super()
		this.x = x
	}

	static parse(name: "h" | "H", s: string) {
		const values = parseValues(s)
		const commands: CommandHorizontal[] = []
		while (values.length) {
			const x = values.shift()!
			const command = new CommandHorizontal({ x })
			command.name = name
			commands.push(command)
		}
		return commands
	}

	mousedown({ x }: Readonly<{ x: number }>) {
		this.offsetX = this.isAbs() ? x - this.x : 0
	}

	move({ x }: Readonly<{ x: number }>) {
		this.x = x - this.offsetX
	}

	toString() {
		return `${this.name} ${this.x}`
	}

	round(dec?: number) {
		this.x = Number(this.x.toFixed(dec))
	}

	scale(ratio: Coord, pathViewBox: RectBox) {
		if (this.isAbs()) {
			const relX = this.x - pathViewBox.x
			this.x = pathViewBox.x + relX * ratio.x
		} else {
			this.x *= ratio.x
		}
	}

	reverseX(svg: Svg) {
		if (this.isAbs()) this.x = svg.getReverseX(this.x)
		else this.x *= -1
	}
}
