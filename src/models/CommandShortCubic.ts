import { barryCenter } from "../mods/barryCenter"
import { parseValues } from "../mods/commands"
import { IsCommand } from "../types/IsCommand"
import { Coord, RectBox } from "../types/Shape"
import { Ctrl } from "./Ctrl"
import { Command } from "./Command"
import { Svg } from "./Svg"

export class CommandShortCubic extends Command {
	name: "s" | "S" = "S"
	ctrls: [Ctrl]
	x = 0
	y = 0

	offsetX = 0
	offsetY = 0

	constructor({ x, y }: Coord) {
		super()
		this.x = x
		this.y = y
		this.ctrls = [new Ctrl(this)]
	}
	mousedown({ x, y }: Readonly<Coord>) {
		this.offsetX = this.isAbs() ? x - this.x : 0
		this.offsetY = this.isAbs() ? y - this.y : 0
		for (const c of this.ctrls) {
			c.mousedown({ x, y })
		}
	}

	move({ x, y }: Readonly<Coord>) {
		this.x = x - this.offsetX
		this.y = y - this.offsetY
		for (const c of this.ctrls) {
			c.move({ x, y })
		}
	}

	static parse(name: "s" | "S", s: string) {
		const values = parseValues(s)
		if (values.length % 4 !== 0) {
			console.error("CommandShortCubic parse will probably failed !", s, values.length)
		}
		const commands: CommandShortCubic[] = []
		while (values.length) {
			const x2 = values.shift()!
			const y2 = values.shift()!
			const x = values.shift()!
			const y = values.shift()!
			const command = new CommandShortCubic({ x, y })
			command.name = name
			command.ctrls = [new Ctrl(command, { x: x2, y: y2 })]
			commands.push(command)
		}
		return commands
	}

	toString() {
		return `${this.name} ${this.ctrls[0].x} ${this.ctrls[0].y}, ${this.x} ${this.y}`
	}

	resetCtrls(bfr: IsCommand & Coord) {
		const c1 = barryCenter(bfr, this, 0.5)
		this.ctrls = [new Ctrl(this, c1)]
	}

	round(dec?: number) {
		this.x = Number(this.x.toFixed(dec))
		this.y = Number(this.y.toFixed(dec))
		for (const ctrl of this.ctrls) {
			ctrl.x = Number(ctrl.x.toFixed(dec))
			ctrl.y = Number(ctrl.y.toFixed(dec))
		}
	}

	scale(ratio: Coord, pathViewBox: RectBox) {
		if (this.isAbs()) {
			const relX = this.x - pathViewBox.x
			this.x = pathViewBox.x + relX * ratio.x
			const relY = this.y - pathViewBox.y
			this.y = pathViewBox.y + relY * ratio.y
			for (const ctrl of this.ctrls) {
				const relX = ctrl.x - pathViewBox.x
				ctrl.x = pathViewBox.x + relX * ratio.x
				const relY = ctrl.y - pathViewBox.y
				ctrl.y = pathViewBox.y + relY * ratio.y
			}
		} else {
			this.x *= ratio.x
			this.y *= ratio.y
			for (const ctrl of this.ctrls) {
				ctrl.x *= ratio.x
				ctrl.y *= ratio.y
			}
		}
	}

	reverseX(svg: Svg) {
		if (this.isAbs()) this.x = svg.getReverseX(this.x)
		else this.x *= -1
		for (const ctrl of this.ctrls) {
			ctrl.x = svg.getReverseX(ctrl.x)
		}
	}

	reverseY(svg: Svg) {
		if (this.isAbs()) this.y = svg.getReverseY(this.y)
		else this.y *= -1
		for (const ctrl of this.ctrls) {
			ctrl.y = svg.getReverseY(ctrl.y)
		}
	}
}
