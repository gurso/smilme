import { parseValues } from "../mods/commands"
import { Coord, RectBox } from "../types/Shape"
import { Command } from "./Command"
import { Svg } from "./Svg"

export class CommandShortQuadratic extends Command {
	name: "t" | "T" = "T"
	x = 0
	y = 0

	offsetX = 0
	offsetY = 0

	constructor({ x, y }: Coord) {
		super()
		this.x = x
		this.y = y
	}

	mousedown({ x, y }: Readonly<Coord>) {
		this.offsetX = this.isAbs() ? x - this.x : 0
		this.offsetY = this.isAbs() ? y - this.y : 0
	}

	move({ x, y }: Coord) {
		this.x = x - this.offsetX
		this.y = y - this.offsetY
	}

	static parse(name: "t" | "T", s: string) {
		const values = parseValues(s)
		if (values.length % 2 !== 0) console.error("CommandMove parse will probably failed !")
		const commands: CommandShortQuadratic[] = []
		while (values.length) {
			const x = values.shift()!
			const y = values.shift()!
			const command = new CommandShortQuadratic({ x, y })
			command.name = name
			commands.push(command)
		}
		return commands
	}

	toString() {
		return `${this.name} ${this.x} ${this.y}`
	}

	round(dec?: number) {
		this.x = Number(this.x.toFixed(dec))
		this.y = Number(this.y.toFixed(dec))
	}

	scale(ratio: Coord, pathViewBox: RectBox) {
		if (this.isAbs()) {
			const relX = this.x - pathViewBox.x
			this.x = pathViewBox.x + relX * ratio.x
			const relY = this.y - pathViewBox.y
			this.y = pathViewBox.y + relY * ratio.y
		} else {
			this.x *= ratio.x
			this.y *= ratio.y
		}
	}

	reverseX(svg: Svg) {
		if (this.isAbs()) this.x = svg.getReverseX(this.x)
		else this.x *= -1
	}
	reverseY(svg: Svg) {
		if (this.isAbs()) this.y = svg.getReverseY(this.y)
		else this.y *= -1
	}
}
