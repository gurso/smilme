import { parseValues } from "../mods/commands"
import { Coord, RectBox } from "../types/Shape"
import { Command } from "./Command"
import { Svg } from "./Svg"

export class CommandVertical extends Command {
	name: "v" | "V" = "V"

	y = 0
	offsetY = 0

	constructor({ y }: { y: number }) {
		super()
		this.y = y
	}
	static parse(name: "v" | "V", s: string) {
		const values = parseValues(s)
		const commands: CommandVertical[] = []
		while (values.length) {
			const y = values.shift()!
			const command = new CommandVertical({ y })
			command.name = name
			commands.push(command)
		}
		return commands
	}

	mousedown({ y }: Readonly<{ y: number }>) {
		this.offsetY = this.isAbs() ? y - this.y : 0
	}

	move({ y }: Readonly<{ y: number }>) {
		this.y = y - this.offsetY
	}

	toString() {
		return `${this.name} ${this.y}`
	}

	round(dec?: number) {
		this.y = Number(this.y.toFixed(dec))
	}

	scale(ratio: Coord, pathViewBox: RectBox) {
		if (this.isAbs()) {
			const relY = this.y - pathViewBox.y
			this.y = pathViewBox.y + relY * ratio.y
		} else {
			this.y *= ratio.y
		}
	}

	reverseY(svg: Svg) {
		if (this.isAbs()) this.y = svg.getReverseY(this.y)
		else this.y *= -1
	}
}
