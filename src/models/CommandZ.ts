import { Command } from "./Command"

export class CommandZ extends Command {
	name: "z" | "Z" = "Z"

	static parse(name: "z" | "Z") {
		const command = new CommandZ()
		command.name = name
		return command
	}

	toString() {
		return this.name
	}

	// TODO ?
	round() {}
	scale() {}
}
