import type { Coord } from "../types/Shape"
import type { IsCommand } from "../types/IsCommand"

export class Ctrl {
	x: number
	y: number

	offsetX = 0
	offsetY = 0

	id = Symbol()

	constructor(
		public command: IsCommand & Coord,
		coord?: Coord,
	) {
		const { x, y } = coord ?? command
		this.x = x
		this.y = y
	}
	mousedown({ x, y }: Readonly<Coord>) {
		this.offsetX = x - this.x
		this.offsetY = y - this.y
	}

	move({ x, y }: Readonly<Coord>) {
		this.x = x - this.offsetX
		this.y = y - this.offsetY
	}
}
