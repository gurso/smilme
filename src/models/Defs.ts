import { parse } from "../mods/attrs"
import { parseFromDom } from "../mods/shape"
import { HasChildren } from "./HasChildren"
import { Tag } from "./Tag"

export class Defs extends HasChildren {
	static fromDom(el: SVGDefsElement) {
		const defs = new Defs()
		const attrs = parse(el)
		defs.setAttrs(attrs)
		return parseFromDom(el, defs)
	}

	constructor(children: Tag[] = []) {
		super("defs", children)
	}
}
