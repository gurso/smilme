import { Attrs, getNumbers, parse } from "../mods/attrs"
import { Coord } from "../types/Shape"
import { Shape } from "./"
import { Svg } from "./Svg"

type EllipseAttrs = {
	cx: number
	cy: number
	rx: number
	ry: number
} & Attrs

export class Ellipse extends Shape {
	static fromDom(el: SVGEllipseElement) {
		const ellipse = new Ellipse()
		const attrs = parse(el)
		ellipse.setAttrs(attrs)
		ellipse.setAttrs(getNumbers(attrs, ["cx", "cy", "rx", "ry"]))
		ellipse.parseAnimation(el)
		return ellipse
	}

	constructor() {
		super("ellipse")
	}

	attrs: EllipseAttrs = {
		cx: 0,
		cy: 0,
		rx: 0,
		ry: 0,
	}

	getViewBox() {
		return {
			x: this.attrs.cx - this.attrs.rx,
			y: this.attrs.cy - this.attrs.ry,
			width: this.attrs.rx * 2,
			height: this.attrs.ry * 2,
		}
	}

	scale({ x, y }: Coord) {
		const minX = this.attrs.cx - this.attrs.rx
		const minY = this.attrs.cy - this.attrs.ry
		const rx = this.attrs.rx * x
		const ry = this.attrs.ry * y
		this.attrs.cx = minX + rx
		this.attrs.cy = minY + ry
		this.attrs.rx = rx
		this.attrs.ry = ry
	}

	mousedown({ x, y }: Coord) {
		this.offsetX = x - this.attrs.cx
		this.offsetY = y - this.attrs.cy
	}

	move({ x, y }: Coord) {
		this.attrs.cx = x - this.offsetX
		this.attrs.cy = y - this.offsetY
	}

	round(dec?: number) {
		this.attrs.rx = Number(this.attrs.rx.toFixed(dec))
		this.attrs.ry = Number(this.attrs.ry.toFixed(dec))
		this.attrs.cx = Number(this.attrs.cx.toFixed(dec))
		this.attrs.cy = Number(this.attrs.cy.toFixed(dec))
		super.round(dec)
	}
	reverseY(svg: Svg) {
		this.attrs.cy = svg.getReverseY(this.attrs.cy)
	}

	reverseX(svg: Svg) {
		this.attrs.cx = svg.getReverseX(this.attrs.cx)
	}
}
