import { parseFromDom } from "../mods/shape"
import { Coord, RectBox } from "../types/Shape"
import { HasChildren } from "./HasChildren"
import { Shape } from "./"
import { Tag } from "./Tag"
import { parse } from "../mods/attrs"
import { getRound } from "../mods/transform"

export class G extends HasChildren {
	static fromDom(el: SVGGElement) {
		const g = new G()
		const attrs = parse(el)
		g.setAttrs(attrs)
		return parseFromDom(el, g)
	}

	constructor(children: Tag[] = [], name = "g") {
		super(name, children)
	}

	lock?: boolean

	scale(ratios: Coord) {
		for (const child of this.children) {
			if (child instanceof Shape || child instanceof G) child.scale(ratios)
		}
	}

	move({ x, y }: Coord) {
		for (const child of this.children) {
			if (child instanceof Shape || child instanceof G) child.move({ x, y })
		}
	}

	mousedown({ x, y }: Coord) {
		for (const child of this.children) {
			if (child instanceof Shape || child instanceof G) child.mousedown({ x, y })
		}
	}

	round(dec?: number) {
		if (this.attrs.transform) this.attrs.transform = getRound(this.attrs.transform)
		for (const child of this.children) {
			if (child instanceof Shape || child instanceof G) {
				child.round(dec)
			}
		}
	}

	getViewBox(): RectBox {
		const children = this.children.filter((child: Tag) => child instanceof Shape || child instanceof G)
		const first = children[0]
		if (!first) throw new Error("no children")
		const vb = first.getViewBox()

		const coord = children.reduce(
			(acc, child) => {
				const current = child.getViewBox()
				if (acc.x > current.x) acc.x = current.x
				if (acc.y > current.y) acc.y = current.y
				if (acc.x2 < current.x + current.width) acc.x2 = current.x + current.width
				if (acc.y2 < current.y + current.height) acc.y2 = current.y + current.height
				return acc
			},
			{ x: vb.x, y: vb.y, x2: 0, y2: 0 },
		)
		const width = coord.x2 - coord.x
		const height = coord.y2 - coord.y
		const { x, y } = coord
		return { x, y, width, height }
	}

	clone() {
		const g = new G()
		for (const child of this.children) {
			if (child instanceof Shape) g.children.push(child.clone())
		}
		return g
	}
}
