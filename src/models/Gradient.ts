import { Tag } from "./Tag"
import { parse, serialize } from "../mods/attrs"
import { getNewId } from "../mods/id"

export type Stop = { color: string; offset: number; opacity: number }
export type Type = "linear" | "radial"

export class Gradient extends Tag {
	static fromDom(el: SVGGradientElement) {
		const type = el instanceof SVGRadialGradientElement ? "radial" : "linear"
		const children = Array.from(el.children)
		const stops: Stop[] = children
			.filter((c): c is SVGStopElement => c instanceof SVGStopElement)
			.map(child => {
				const op = child.getAttribute("stop-opacity")
				const opacity = op ? Number(op) : 1
				const color = child.getAttribute("stop-color")!
				const offset = Number(child.getAttribute("offset"))
				return { color, offset, opacity }
			})
		const gradient = new Gradient(type, stops)
		gradient.setAttrs(parse(el))
		return gradient
	}

	attrs: { id: string }

	constructor(
		public type: Type = "linear",
		public stops: Stop[] = [],
	) {
		super("gradient")
		const id = "gradient" + getNewId()
		this.attrs = { id }
	}

	rmStop(stop: Stop) {
		const i = this.stops.indexOf(stop)
		if (i >= 0) this.stops.splice(i, 1)
		else console.warn("[Gradient][rmStop] stop not found")
	}

	toCss() {
		// TODO: not handle opacity
		const s = this.stops.map(({ color, offset }) => color + " " + offset * 100 + "%")
		return this.type + "-gradient(" + s.join(",") + ")"
	}

	toString() {
		const stopStrings = this.stops
			.map(stop => {
				const op = stop.opacity === 1 ? "" : ` stop-opacity="${stop.opacity}"`
				return `<stop offset="${stop.offset}" stop-color="${stop.color}"${op}></stop>`
			})
			.join("")
		const tag = this.type + "Gradient"
		return `<${tag} ${serialize(this.attrs)}>${stopStrings}</${tag}>`
	}
}
