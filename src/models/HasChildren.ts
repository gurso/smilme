import { Shape, Tag } from "./"
import { Svg } from "./Svg"
import { parse, serialize } from "../mods/attrs"
import { parseFromDom } from "../mods/shape"

export type AConstructorTypeOf<T> = (...args: unknown[]) => T

export class HasChildren extends Tag {
	static fromDom(el: SVGElement) {
		const tag = new HasChildren("HasChildren", [], el.tagName)
		tag.setAttrs(parse(el))
		return parseFromDom(el, tag)
	}

	constructor(
		name: string,
		public children: Tag[] = [],
		public html = name,
	) {
		super(name, html)
		this.children = [...children]
	}

	add(...tags: Tag[]) {
		this.children.push(...tags)
	}

	listOf<T extends Tag>(classConstructor: AConstructorTypeOf<T>): T[] {
		const list: T[] = []
		if (this instanceof classConstructor) list.push(this as unknown as T)
		for (const child of this.children) {
			if (child instanceof HasChildren) list.push(...child.listOf(classConstructor))
			else if (child instanceof classConstructor) list.push(child as unknown as T)
		}
		return list
	}

	rm(...tags: Tag[]) {
		this.children = this.children.filter(child => !tags.includes(child))
		for (const child of this.children) {
			if (child instanceof HasChildren) child.rm(...tags)
		}
	}

	replace(old: Tag, ...by: Tag[]) {
		const i = this.children.indexOf(old)
		if (i < 0) {
			for (const child of this.children) {
				if (child instanceof HasChildren) {
					const r = child.replace(old, ...by)
					if (r) return true
				}
			}
		} else {
			this.children.splice(i, 1, ...by)
			return true
		}
	}

	insertAfter(newTag: Tag, target: Tag) {
		const i = this.children.indexOf(target)
		if (i < 0) {
			for (const child of this.children) {
				if (child instanceof HasChildren) {
					const r = child.insertAfter(newTag, target)
					if (r) return true
				}
			}
		} else {
			this.children.splice(i + 1, 0, newTag)
			return true
		}
	}

	isParentOf(...maybeChildren: Tag[]) {
		return maybeChildren.every(maybeChild => this.children.includes(maybeChild))
	}

	isAncestorOf(...maybeChildren: Tag[]): boolean {
		return maybeChildren.every(
			maybeChild =>
				this.children.includes(maybeChild) ||
				this.children.some(child => child instanceof HasChildren && child.isAncestorOf(maybeChild)),
		)
	}

	someDescendant(cb: (tag: Tag) => unknown) {
		for (const child of this.children) {
			if (cb(child)) return true
			if (child instanceof HasChildren) {
				if (child.someDescendant(cb)) return true
			}
		}
		return false
	}

	getPathTo(tag: HasChildren): HasChildren[] {
		const path: HasChildren[] = [this]
		for (const child of this.children) {
			if (child instanceof HasChildren && child.isAncestorOf(tag)) {
				path.push(...child.getPathTo(tag))
				break
			}
		}
		return path
	}

	reverseX(svg: Svg) {
		for (const child of this.children) {
			if (child instanceof Shape || child instanceof HasChildren) child.reverseX(svg)
		}
	}

	reverseY(svg: Svg) {
		for (const child of this.children) {
			if (child instanceof Shape || child instanceof HasChildren) child.reverseY(svg)
		}
	}

	toString() {
		return `<${this.html} ${serialize(this.attrs)}>${this.children.join("")}</${this.html}>`
	}
}
