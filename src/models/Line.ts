import { Attrs, getNumbers, parse } from "../mods/attrs"
import { Coord } from "../types/Shape"
import { Shape } from "./"
import { Svg } from "./Svg"

type LineAttrs = {
	x1: number
	y1: number
	x2: number
	y2: number
} & Attrs

export class Line extends Shape {
	static fromDom(el: SVGLineElement) {
		const line = new Line()
		const attrs = parse(el)
		line.setAttrs(attrs)
		line.setAttrs(getNumbers(attrs, ["x1", "y1", "x2", "y2"]))
		return line
	}
	constructor() {
		super("line")
	}

	attrs: LineAttrs = {
		x1: 0,
		y1: 0,
		x2: 0,
		y2: 0,
	}

	offsetX2 = 0
	offsetY2 = 0

	getViewBox() {
		const x = Math.min(this.attrs.x1, this.attrs.x2)
		const y = Math.min(this.attrs.y1, this.attrs.y2)
		return {
			x,
			y,
			width: Math.max(this.attrs.x1, this.attrs.x2) - x,
			height: Math.max(this.attrs.y1, this.attrs.y2) - y,
		}
	}

	scale({ x, y }: Coord) {
		const minX = Math.min(this.attrs.x1, this.attrs.x2)
		const minY = Math.min(this.attrs.y1, this.attrs.y2)
		const x1 = (this.attrs.x1 - minX) * x
		const x2 = (this.attrs.x2 - minX) * x
		const y1 = (this.attrs.y1 - minY) * y
		const y2 = (this.attrs.y2 - minY) * y
		this.attrs.x1 = minX + x1
		this.attrs.x2 = minX + x2
		this.attrs.y1 = minY + y1
		this.attrs.y2 = minY + y2
	}

	mousedown({ x, y }: Coord) {
		this.offsetX = x - this.attrs.x1
		this.offsetY = y - this.attrs.y1
		this.offsetX2 = x - this.attrs.x2
		this.offsetY2 = y - this.attrs.y2
	}

	move({ x, y }: Coord) {
		this.attrs.x1 = x - this.offsetX
		this.attrs.y1 = y - this.offsetY
		this.attrs.x2 = x - this.offsetX2
		this.attrs.y2 = y - this.offsetY2
	}

	round(dec?: number) {
		this.attrs.x1 = Number(this.attrs.x1.toFixed(dec))
		this.attrs.y1 = Number(this.attrs.y1.toFixed(dec))
		this.attrs.x2 = Number(this.attrs.x2.toFixed(dec))
		this.attrs.y2 = Number(this.attrs.y2.toFixed(dec))
		super.round(dec)
	}

	reverseY(svg: Svg) {
		this.attrs.y1 = svg.getReverseY(this.attrs.y1)
		this.attrs.y2 = svg.getReverseY(this.attrs.y2)
	}

	reverseX(svg: Svg) {
		this.attrs.x1 = svg.getReverseX(this.attrs.x1)
		this.attrs.x2 = svg.getReverseX(this.attrs.x2)
	}
}
