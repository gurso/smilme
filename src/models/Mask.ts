import { Attrs, parse } from "../mods/attrs"
import { getNewId } from "../mods/id"
import { parseFromDom } from "../mods/shape"
import { HasChildren } from "./HasChildren"
import { Tag } from "./Tag"

type MaskAttrs = { id: string; maskUnits?: "userSpaceOnUse" | "objectBoundingBox" } & Attrs

export class Mask extends HasChildren {
	static fromDom(el: SVGDefsElement) {
		const defs = new Mask()
		const attrs = parse(el)
		defs.setAttrs(attrs)
		return parseFromDom(el, defs)
	}

	constructor(children: Tag[] = []) {
		super("mask", children)
	}

	attrs: MaskAttrs = { id: "mask-ils-" + getNewId() }
}
