import { Svg } from "./Svg"
import type { IsCommand } from "../types/IsCommand"
import type { Coord } from "../types/Shape"
import { CommandZ } from "./CommandZ"
import { Shape } from "./"
import { isSafeNumber } from "../mods/nb"
import { parsePathD, serializePathD } from "../mods/path"
import { parse } from "../mods/attrs"

export class Path extends Shape {
	static fromDom(el: SVGPathElement) {
		const path = new Path()
		const commands = parsePathD(el.getAttribute("d") ?? "")
		path.commands = commands
		const attrs = parse(el)
		delete attrs.d
		path.setAttrs(attrs)
		return path
	}

	commands: IsCommand[] = []

	constructor() {
		super("path")
	}

	toString() {
		const d = serializePathD(this.commands)
		return super.toString({ d })
	}

	isLastCommand(command: IsCommand) {
		return this.commands.at(-1)?.id === command.id
	}

	getCommandAfter(command: IsCommand) {
		const i = this.commands.findIndex(c => c.id === command.id)
		return this.commands[(i + 1) % this.commands.length]
	}

	getCommandBefore(command: IsCommand) {
		const i = this.commands.findIndex(c => c.id === command.id)
		return this.commands[i - 1]
	}

	getIndexCommand(command: IsCommand) {
		return this.commands.findIndex(c => c.id === command.id)
	}

	getAbsCoord(axis: "x" | "y", c?: IsCommand): number {
		const first = this.commands[0]
		return c
			? c instanceof CommandZ
				? (first?.[axis] ?? 0)
				: c.isAbs() && axis in c && isSafeNumber(c[axis])
					? c[axis]
					: !c.isAbs() && axis in c && isSafeNumber(c[axis])
						? c[axis] + this.getAbsCoord(axis, this.getCommandBefore(c))
						: this.getAbsCoord(axis, this.getCommandBefore(c))
			: 0
	}

	getAbsCoords(c?: Readonly<IsCommand>) {
		return {
			x: this.getAbsCoord("x", c),
			y: this.getAbsCoord("y", c),
		}
	}

	rmCommand(command: IsCommand) {
		const i = this.commands.findIndex(c => c.id === command.id)
		if (i >= 0) {
			this.commands.splice(i, 1)
		}
	}

	mousedown(coord: Coord) {
		for (const command of this.commands) {
			if (!(command instanceof CommandZ)) command.mousedown(coord)
		}
	}

	move(coord: Coord) {
		for (const command of this.commands) {
			if ((command.isAbs() || command === this.commands[0]) && !(command instanceof CommandZ)) command.move(coord)
		}
	}

	getCoord() {
		const { x = 0, y = 0 } = this.commands[0] ?? { x: 0, y: 0 }
		return { x, y }
	}

	getViewBox() {
		const first = this.commands[0]
		if (!first) throw new Error("Cannot compute rectBox from empty Path")
		const { x, y } = this.getAbsCoords(first)
		const tmp = { x, y, x2: x, y2: y }
		for (const command of this.commands) {
			const abs = this.getAbsCoords(command)
			tmp.x = Math.min(tmp.x, abs.x)
			tmp.x2 = Math.max(tmp.x2, abs.x)
			tmp.y = Math.min(tmp.y, abs.y)
			tmp.y2 = Math.max(tmp.y2, abs.y)
		}
		return {
			x: tmp.x,
			y: tmp.y,
			width: tmp.x2 - tmp.x,
			height: tmp.y2 - tmp.y,
		}
	}

	scale(ratio: Coord) {
		for (const command of this.commands) {
			command.scale(ratio, this.getViewBox())
		}
	}

	round(dec?: number) {
		for (const command of this.commands) {
			command.round(dec)
		}
		super.round(dec)
	}

	reverseX(svg: Svg) {
		for (const command of this.commands) {
			if ("reverseX" in command) command.reverseX(svg)
		}
	}

	reverseY(svg: Svg) {
		for (const command of this.commands) {
			if ("reverseY" in command) command.reverseY(svg)
		}
	}

	clone() {
		const p = super.clone()
		for (const command of this.commands) {
			p.commands.push(command.clone())
		}
		return p
	}
}
