import { Coord } from "../types/Shape"

export class Point {
	readonly id = Symbol()
	x: number
	y: number

	offsetX = 0
	offsetY = 0

	constructor(coord: Coord) {
		const { x, y } = coord
		this.x = x
		this.y = y
	}
	mousedown({ x, y }: Coord) {
		this.offsetX = x - this.x
		this.offsetY = y - this.y
	}

	move({ x, y }: Coord) {
		this.x = x - this.offsetX
		this.y = y - this.offsetY
	}

	toString() {
		return `${this.x},${this.y}`
	}
}
