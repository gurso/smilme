import { Coord } from "../types/Shape"
import { Shape } from "./"
import { Point } from "./Point"
import { Svg } from "./Svg"
import { parse } from "../mods/attrs"

export class Poly extends Shape {
	static fromDom(el: SVGPolygonElement | SVGPolylineElement) {
		const points = Array.from(el.points).map(p => new Point(p))
		const poly = new Poly()
		poly.points.push(...points)
		const attrs = parse(el)
		delete attrs.point
		poly.setAttrs(attrs)
		poly.html = el instanceof SVGPolygonElement ? "polygon" : "polyline"
		return poly
	}
	points: Point[] = []

	constructor() {
		super("poly", "polyline")
	}

	mousedown(coord: Coord) {
		for (const point of this.points) {
			point.mousedown(coord)
		}
	}
	move(coord: Coord) {
		for (const point of this.points) {
			point.move(coord)
		}
	}

	getViewBox() {
		const first = this.points[0]
		if (!first) throw new Error("Cannot compute rectBox from empty Poly")
		const tmp = { x: first.x, y: first.y, x2: first.x, y2: first.y }
		for (const p of this.points) {
			tmp.x = Math.min(tmp.x, p.x)
			tmp.y = Math.min(tmp.y, p.y)
			tmp.x2 = Math.max(tmp.x2, p.x)
			tmp.y2 = Math.max(tmp.y2, p.y)
		}
		return {
			x: tmp.x,
			y: tmp.y,
			width: tmp.x2 - tmp.x,
			height: tmp.y2 - tmp.y,
		}
	}

	getCoord() {
		const { x, y } = this.points[0] ?? { x: 0, y: 0 }
		return { x, y }
	}

	scale({ x, y }: Coord) {
		const { x: minX, y: minY } = this.getViewBox()
		for (const point of this.points) {
			const relX = point.x - minX
			const relY = point.y - minY
			point.x = minX + relX * x
			point.y = minY + relY * y
		}
	}

	toString() {
		const points = this.points.join(" ")
		return super.toString({ points })
	}

	round(dec?: number) {
		for (const point of this.points) {
			point.x = Number(point.x.toFixed(dec))
			point.y = Number(point.y.toFixed(dec))
		}
		super.round(dec)
	}

	reverseX(svg: Svg) {
		for (const point of this.points) {
			point.x = svg.getReverseX(point.x)
		}
	}

	reverseY(svg: Svg) {
		for (const point of this.points) {
			point.y = svg.getReverseY(point.y)
		}
	}

	clone() {
		const p = super.clone()
		for (const point of this.points) {
			p.points.push(new Point(point))
		}
		return p
	}
}
