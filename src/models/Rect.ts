import { Coord } from "../types/Shape"
import { CommandLine } from "./CommandLine"
import { CommandMove } from "./CommandMove"
import { Path, Shape } from "./"
import { Svg } from "./Svg"
import { Attrs, getNumbers, parse } from "../mods/attrs"

type RectAttrs = {
	x: number
	y: number
	width: number
	height: number
	rx?: number
	ry?: number
} & Attrs

export class Rect extends Shape {
	static fromDom(el: SVGRectElement) {
		const rect = new Rect()
		const attrs = parse(el)
		rect.setAttrs(attrs)
		rect.setAttrs(getNumbers(attrs, ["x", "y", "width", "height", "rx", "ry"]))
		return rect
	}

	constructor() {
		super("rect")
	}

	attrs: RectAttrs = {
		x: 0,
		y: 0,
		width: 0,
		height: 0,
		rx: 0,
		ry: 0,
	}

	getViewBox() {
		return {
			x: this.attrs.x,
			y: this.attrs.y,
			width: this.attrs.width,
			height: this.attrs.height,
		}
	}

	scale({ x, y }: Coord) {
		this.attrs.width *= x
		this.attrs.height *= y
	}

	mousedown({ x, y }: Coord) {
		this.offsetX = x - this.attrs.x
		this.offsetY = y - this.attrs.y
	}

	move({ x, y }: Coord) {
		this.attrs.x = x - this.offsetX
		this.attrs.y = y - this.offsetY
	}

	round(dec?: number) {
		this.attrs.x = Number(this.attrs.x.toFixed(dec))
		this.attrs.y = Number(this.attrs.y.toFixed(dec))
		this.attrs.width = Number(this.attrs.width.toFixed(dec))
		this.attrs.height = Number(this.attrs.height.toFixed(dec))
		super.round(dec)
	}
	reverseY(svg: Svg) {
		this.attrs.y = svg.getReverseY(this.attrs.y) - this.attrs.height
	}

	reverseX(svg: Svg) {
		this.attrs.x = svg.getReverseX(this.attrs.x) - this.attrs.width
	}

	toPath() {
		const path = new Path()
		// TODO: rx,ry...
		const { x, y, width, height, ...attrs } = this.attrs
		path.commands.push(new CommandMove({ x, y }))
		path.commands.push(new CommandLine({ x: x + width, y }))
		path.commands.push(new CommandLine({ x: x + width, y: y + height }))
		path.commands.push(new CommandLine({ x, y: y + height }))
		Object.assign(path.attrs, attrs)
		return path
	}
}
