import { Coord, RectBox } from "../types/Shape"
import { Animation } from "../types/Animation"
import { Tag } from "./Tag"
import { AnimateMotion } from "./"
import { Animate } from "./Animate"
import { AnimateTransform } from "./AnimateTransform"
import { Svg } from "./Svg"
import { Attrs, serialize } from "../mods/attrs"
import { getRound } from "../mods/transform"

export abstract class Shape extends Tag {
	parseAnimation(el: SVGElement) {
		const animates = Array.from(el.querySelectorAll("animate")).map(Animate.fromDom)
		const animateTransform = Array.from(el.querySelectorAll("animateTransform")).map(AnimateTransform.fromDom)
		this.animates = animates.concat(animateTransform)
		const motion = el.querySelector("animateMotion") ?? undefined
		this.motion = motion && AnimateMotion.fromDom(motion)
	}

	animates: Animation[] = []
	motion?: AnimateMotion

	lock?: boolean
	offsetX = 0
	offsetY = 0

	// TODO: rename setOffset(s) ?
	abstract mousedown(coord: Coord): void
	abstract move(coord: Coord): void
	abstract scale(coord: Coord): void
	abstract getViewBox(): RectBox
	abstract reverseY(svg: Svg): void
	abstract reverseX(svg: Svg): void

	// TODO: rename getAnchor substract?
	getCoord() {
		const x = Number(this.attrs.x ?? this.attrs.cx ?? this.attrs.x1)
		const y = Number(this.attrs.y ?? this.attrs.cy ?? this.attrs.y1)
		return { x, y }
	}

	round(dec?: number) {
		if (this.attrs.transform) {
			this.attrs.transform = getRound(this.attrs.transform, dec)
		}
		if (this.motion) {
			this.motion.path.round()
		}
	}

	toString(attrs?: Attrs, content = "") {
		const moreAttrs = attrs ? serialize(attrs) : ""
		const motion = this.motion?.toString() ?? ""
		return `<${this.html} ${serialize(this.attrs)} ${moreAttrs}>${this.animates.join("")}${motion}${content}</${this.html}>`
	}

	clone() {
		const c = new (this.constructor as new () => this)()
		c.setAttrs(this.attrs)
		// TODO animation/motion ??
		return c
	}
}
