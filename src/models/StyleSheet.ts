import { Tag } from "./Tag"

export class StyleSheetTag extends Tag {
	constructor(public el: SVGStyleElement) {
		super("style-sheet", "style")
	}

	toString() {
		return this.el.outerHTML
	}
}
