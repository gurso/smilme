import { parse, Attrs, getNumbers, serialize } from "../mods/attrs"
import { parseFromDom } from "../mods/shape"
import { toTuple } from "../mods/tuple"
import { Coord } from "../types/Shape"
import { G } from "./G"
import { Tag } from "./Tag"

export type Align =
	| "xMinYMin"
	| "xMidYMin"
	| "xMaxYMin"
	| "xMinYMid"
	| "xMidYMid"
	| "xMaxYMid"
	| "xMinYMax"
	| "xMidYMax"
	| "xMaxYMax"
export type MeetOrSlice = "meet" | "slice"
type PreserveAspectRatio = "none" | Align | `${Align} ${MeetOrSlice}`

type SvgAttrs = {
	width: number
	height: number
	viewBox: string
	preserveAspectRatio?: PreserveAspectRatio
} & Attrs

export class Svg extends G {
	static fromDom(el: SVGSVGElement) {
		const svg = new Svg()
		const attrs = parse(el)
		delete attrs.xmlns
		delete attrs.version
		svg.setAttrs(attrs)
		svg.setAttrs(getNumbers(attrs, ["width", "height"]))
		return parseFromDom(el, svg)
	}

	constructor(children: Tag[] = []) {
		super(children, "svg")
	}

	attrs: SvgAttrs = {
		width: 360,
		height: 360,
		viewBox: "0 0 360 360",
	}
	title = ""

	scale({ x, y }: Coord) {
		this.attrs.width *= x
		this.attrs.height *= y
	}

	toString() {
		const titleStr = this.title ? `<title>${this.title}</title>` : ""
		const xmlns = ' xmlns="http://www.w3.org/2000/svg"'
		return `<svg${xmlns} ${serialize(this.attrs)}>${titleStr}${this.children.join("")}</svg>`
			.replaceAll(/\s{2,}/g, " ")
			.replaceAll(" >", ">")
			.trim()
	}

	getReverseY(y: number) {
		const [, , , height] = toTuple(this.attrs.viewBox.split(" ").map(Number) ?? [], 4)
		return height - y
	}

	getReverseX(x: number) {
		const [, , width] = toTuple(this.attrs.viewBox.split(" ").map(Number) ?? [], 4)
		return width - x
	}

	reverseX() {
		super.reverseX(this)
	}

	reverseY() {
		super.reverseY(this)
	}
}
