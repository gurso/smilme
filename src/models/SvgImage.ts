import { Shape } from "./Shape"
import { Attrs, getNumbers, parse } from "../mods/attrs"
import { Coord, RectBox } from "../types/Shape"
import { Svg } from "./Svg"

export type ImageAttrs = Attrs & RectBox & { href: string }

export class SvgImage extends Shape {
	static fromDom(el: SVGImageElement) {
		const image = new SvgImage()
		const attrs = parse(el)
		image.setAttrs(attrs)
		image.setAttrs(getNumbers(attrs, ["x", "y", "width", "height"]))
		return image
	}

	ignore?: boolean

	attrs: ImageAttrs = {
		x: 0,
		y: 0,
		width: 100,
		height: 100,
		href: "",
	}

	public constructor() {
		super("image")
	}

	toString() {
		if (this.ignore) return ""
		else return super.toString()
	}

	getViewBox() {
		return {
			x: this.attrs.x,
			y: this.attrs.y,
			width: this.attrs.width,
			height: this.attrs.height,
		}
	}

	scale({ x, y }: Coord) {
		this.attrs.width *= x
		this.attrs.height *= y
	}

	mousedown({ x, y }: Coord) {
		this.offsetX = x - this.attrs.x
		this.offsetY = y - this.attrs.y
	}

	move({ x, y }: Coord) {
		this.attrs.x = x - this.offsetX
		this.attrs.y = y - this.offsetY
	}

	round(dec?: number) {
		this.attrs.x = Number(this.attrs.x.toFixed(dec))
		this.attrs.y = Number(this.attrs.y.toFixed(dec))
		this.attrs.width = Number(this.attrs.width.toFixed(dec))
		this.attrs.height = Number(this.attrs.height.toFixed(dec))
		super.round(dec)
	}

	reverseY(svg: Svg) {
		this.attrs.y = svg.getReverseY(this.attrs.y) - this.attrs.height
	}

	reverseX(svg: Svg) {
		this.attrs.x = svg.getReverseX(this.attrs.x) - this.attrs.width
	}
}
