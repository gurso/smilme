import { HasChildren } from "."
import { Attrs, parse, serialize } from "../mods/attrs"

export class Tag {
	static fromDom(el: Element) {
		const tag = new Tag("tag", el.tagName)
		tag.setAttrs(parse(el))
		return tag
	}

	readonly id = Symbol()
	readonly component: string
	readonly form: string

	attrs: Attrs = {}
	hidden?: boolean

	constructor(
		public name: string,
		public html = name,
	) {
		this.component = name + "-shape"
		this.form = name + "-form"
	}

	/**
	 *	this functions use ts limitation to overide types defined in each Shape children
	 */
	setAttrs(attrs: Attrs) {
		this.attrs = { ...this.attrs, ...attrs }
	}
	rmAttrs(...attrs: string[]) {
		for (const attr of attrs) {
			delete this.attrs[attr]
		}
	}
	getAttr(name: string) {
		return this.attrs[name]
	}

	getParent(ancestor: HasChildren): HasChildren | undefined {
		if (ancestor.isParentOf(this)) {
			return ancestor
		} else if (ancestor.isAncestorOf(this)) {
			for (const child of ancestor.children) {
				if (child instanceof HasChildren) {
					const parent = this.getParent(child)
					if (parent) return parent
				}
			}
		}
	}

	toString() {
		return `<${this.html} ${serialize(this.attrs)}></${this.html}>`
	}
}
