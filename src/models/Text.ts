import { Attrs, getNumbers, parse } from "../mods/attrs"
import { getTextSize } from "../mods/text"
import { Coord } from "../types/Shape"
import { Shape } from "./"
import { Svg } from "./Svg"

type TextAttrs = {
	x: number
	y: number
	dx?: number
	dy?: number
	textAnchor?: "start" | "middle" | "end"
	dominantBaseline?: "auto" | "middle" | "text-bottom" | "text-top" | "central" | "hanging" | "alphabetic"
	textLength?: number
	lengthAdjust?: "spacing" | "spacingAndGlyphs"
}

// Text is not a real Shape but more convenient to considare as it
export class Text extends Shape {
	static fromDom(el: SVGTextElement) {
		const txt = new Text()
		const attrs = parse(el)
		txt.setAttrs(attrs)
		txt.setAttrs(getNumbers(attrs, ["x", "y"]))
		txt.content = el.textContent ?? ""
		return txt
	}
	content = ""

	attrs: TextAttrs & Attrs = {
		x: 0,
		y: 0,
	}

	constructor() {
		super("text")
	}

	getViewBox() {
		const { width, height } = getTextSize(this)
		return {
			x: this.attrs.x,
			y: this.attrs.y - height,
			width: this.attrs.textLength ?? width,
			height,
		}
	}

	scale() {
		console.warn("Exeption not scalable ??")
	}

	mousedown({ x, y }: Coord) {
		this.offsetX = x - this.attrs.x
		this.offsetY = y - this.attrs.y
	}

	move({ x, y }: Coord) {
		this.attrs.x = x - this.offsetX
		this.attrs.y = y - this.offsetY
	}

	toString() {
		return super.toString({}, this.content)
	}

	round(dec?: number) {
		this.attrs.x = Number(this.attrs.x.toFixed(dec))
		this.attrs.y = Number(this.attrs.y.toFixed(dec))
		super.round(dec)
	}

	reverseX(svg: Svg) {
		this.attrs.x = svg.getReverseX(this.attrs.x)
	}

	reverseY(svg: Svg) {
		this.attrs.y = svg.getReverseY(this.attrs.y)
	}
}
