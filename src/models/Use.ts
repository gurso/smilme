import { Tag } from "./Tag"
import { parse } from "../mods/attrs"

export class Use extends Tag {
	static fromDom(el: SVGUseElement) {
		const use = new Use()
		use.setAttrs(parse(el))
		return use
	}

	constructor() {
		super("use")
	}

	attrs: { href: string } = { href: "" }
}
