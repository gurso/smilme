export type Attrs = Record<string, number | string> & { style?: string; transform?: string }

export function parse(el: Element) {
	const attrs: Attrs = {}
	for (const attr of Object.values(el.attributes)) {
		attrs[attr.name] = attr.value
	}
	return attrs
}

export function getNumbers<T extends string>(attrs: Record<string, string | number>, list: T[]) {
	const r: Partial<Record<T, number>> = {}
	for (const k of list) {
		const nb = Number(attrs[k])
		if (Number.isFinite(nb)) r[k] = nb
	}
	return r as Record<T, number>
}

export function serialize(o: Record<string, string | number>) {
	return Object.entries(o)
		.filter(([, v]) => v || v === 0)
		.map(([k, v]) => `${k}="${v}"`)
		.join(" ")
}
