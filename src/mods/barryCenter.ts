import { Coord } from "../types/Shape"

export function barryCenter(from: Readonly<Coord>, to: Readonly<Coord>, fraction: Readonly<number> = 0.5) {
	const x = to.x + (from.x - to.x) * fraction
	const y = to.y + (from.y - to.y) * fraction
	return { x, y }
}
