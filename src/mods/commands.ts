export const COMMAND_NAMES = ["a", "c", "h", "l", "m", "q", "s", "t", "v", "z"] as const

type LowerCommang = (typeof COMMAND_NAMES)[number]
export type CommandName = LowerCommang | Uppercase<LowerCommang>

export function parseValues(s: string, isArcCommand = false) {
	const r: number[] = []
	let nip = ""
	for (const l of s.trim()) {
		if (
			l === " " ||
			l === "," ||
			(l === "." && nip.includes(".")) ||
			// (/\d/.test(l) && nip === "0") || // not sur of this
			(l === "-" && nip) ||
			(isArcCommand && r.length % 7 > 2 && r.length % 7 < 5 && (nip === "0" || nip === "1"))
		) {
			if (nip.length) {
				r.push(Number(nip))
				nip = ""
			}
		}
		nip += l.trim().replace(",", "")
	}
	if (nip) r.push(Number(nip))
	return r
}
