import { Coord } from "../types/Shape"

export function add(...coords: Coord[]) {
	return coords.reduce(
		(p, c) => {
			const x = p.x + c.x
			const y = p.y + c.y
			return { x, y }
		},
		{ x: 0, y: 0 },
	)
}

export function substract(from: Coord, ...toSub: Coord[]) {
	const { x: _x, y: _y } = add(...toSub)
	return {
		x: from.x - _x,
		y: from.y - _y,
	}
}

export function reverse(coord: Coord) {
	return {
		x: coord.x * -1,
		y: coord.y * -1,
	}
}
