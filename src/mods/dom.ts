export function onMouseMoveUntilUp(onMove: (e: MouseEvent) => void, onUp?: (e: MouseEvent) => void) {
	window.addEventListener("mousemove", onMove)
	const msup = (e: MouseEvent) => {
		if (onUp) onUp(e)
		window.removeEventListener("mousemove", onMove)
		window.removeEventListener("mouseup", msup)
	}
	window.addEventListener("mouseup", msup)
}
