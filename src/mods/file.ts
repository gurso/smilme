import { Svg } from "../models/Svg"
import { toTuple } from "./tuple"

export function parseSvgStr(svgStr: string) {
	const minified = svgStr
		.replace(/^.+\?>/, "")
		.replace(/\s+/g, " ")
		.replaceAll("> <", "><")
	const parser = new DOMParser()
	const doc = parser.parseFromString(minified, "text/html")
	const svgEl = doc.querySelector<SVGSVGElement>("svg")
	if (!svgEl) throw new Error("Failed to parse string")
	const w = Number(svgEl.getAttribute("width"))
	const h = Number(svgEl.getAttribute("height"))
	const vb = toTuple(svgEl.getAttribute("viewBox")?.split(" ").map(Number) ?? [0, 0, w, h], 4)
	const svg = Svg.fromDom(svgEl)
	svg.attrs.width = w || vb[2] - vb[0]
	svg.attrs.height = h || vb[3] - vb[1]
	svg.attrs.viewBox = vb.join(" ")
	return svg
}

export async function fileToBase64(file: File) {
	return new Promise<string>((resolve, reject) => {
		const reader = new FileReader()
		reader.readAsDataURL(file)
		reader.onload = () => {
			if (reader.result instanceof ArrayBuffer) {
				reject("TODO")
			} else if (reader.result) {
				resolve(reader.result)
			} else reject("null is not a valid data url")
		}
		reader.onerror = reject
	})
}
