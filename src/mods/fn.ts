type Cb = (...args: unknown[]) => void

export function debounce(cb: Cb, ms = 0) {
	let to: ReturnType<typeof setTimeout>

	return function (...args: unknown[]) {
		if (to) clearTimeout(to)
		to = setTimeout(() => cb(...args), ms)
	}
}
