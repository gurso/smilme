function* idGenerator() {
	let index = 0
	while (true) yield ++index
}

export function makeIdMaker() {
	const generator = idGenerator()
	return function () {
		const result = generator.next()
		if (result.done) return 0
		return result.value
	}
}

export const getNewId = makeIdMaker()
