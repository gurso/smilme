export const isSafeNumber = Number.isFinite as (n: unknown) => n is number

export function clamp(min: number, n: number, max: number) {
	return Math.min(Math.max(n, min), max)
}
