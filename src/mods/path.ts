import { CommandArc } from "../models/CommandArc"
import { CommandCubic } from "../models/CommandCubic"
import { CommandHorizontal } from "../models/CommandHorizontal"
import { CommandLine } from "../models/CommandLine"
import { CommandMove } from "../models/CommandMove"
import { CommandQuadratic } from "../models/CommandQuadratic"
import { CommandShortCubic } from "../models/CommandShortCubic"
import { CommandShortQuadratic } from "../models/CommandShortQuadratic"
import { CommandVertical } from "../models/CommandVertical"
import { CommandZ } from "../models/CommandZ"
import { IsCommand } from "../types/IsCommand"

export function parsePathD(d: string) {
	const arr = d.split(/(?=[a-zA-Z])/)
	const commands: IsCommand[] = []
	for (const s of arr) {
		const commandName = s[0]
		const values = s.slice(1)
		switch (commandName) {
			case "A":
			case "a":
				commands.push(...CommandArc.parse(commandName, values))
				break
			case "c":
			case "C":
				commands.push(...CommandCubic.parse(commandName, values))
				break
			case "H":
			case "h":
				commands.push(...CommandHorizontal.parse(commandName, values))
				break
			case "l":
			case "L":
				commands.push(...CommandLine.parse(commandName, values))
				break
			case "m":
			case "M":
				commands.push(...CommandMove.parse(commandName, values))
				break
			case "q":
			case "Q":
				commands.push(...CommandQuadratic.parse(commandName, values))
				break
			case "s":
			case "S":
				commands.push(...CommandShortCubic.parse(commandName, values))
				break
			case "t":
			case "T":
				commands.push(...CommandShortQuadratic.parse(commandName, values))
				break
			case "v":
			case "V":
				commands.push(...CommandVertical.parse(commandName, values))
				break
			case "Z":
			case "z":
				commands.push(CommandZ.parse(commandName))
				break
		}
	}
	return commands
}

export function serializePathD(commands: IsCommand[]) {
	return commands.join(" ")
}
