import { Circle } from "../models/Circle"
import { Ellipse } from "../models/Ellipse"
import { Line } from "../models/Line"
import { Poly } from "../models/Poly"
import { Rect } from "../models/Rect"
import { Text } from "../models/Text"
import { G } from "../models/G"
import { Svg } from "../models/Svg"
import { Defs } from "../models/Defs"
import { Tag } from "../models/Tag"
import { Gradient } from "../models/Gradient"
import { StyleSheetTag } from "../models/StyleSheet"
import { HasChildren } from "../models/HasChildren"
import { SvgImage } from "../models/SvgImage"
import { Mask } from "../models/Mask"
import { Path } from "../models"

export function parseFromDom<T extends HasChildren>(el: SVGElement, parent: T) {
	for (const child of Array.from(el.children)) {
		switch (child.constructor) {
			case SVGSVGElement:
				parent.children.push(Svg.fromDom(child as SVGSVGElement))
				break
			case SVGGElement:
				parent.children.push(G.fromDom(child as SVGGElement))
				break
			case SVGMaskElement:
				parent.children.push(Mask.fromDom(child as SVGDefsElement))
				break
			case SVGDefsElement:
				parent.children.push(Defs.fromDom(child as SVGDefsElement))
				break
			case SVGImageElement:
				parent.children.push(SvgImage.fromDom(child as SVGImageElement))
				break
			case SVGPathElement:
				parent.children.push(Path.fromDom(child as SVGPathElement))
				break
			case SVGRectElement:
				parent.children.push(Rect.fromDom(child as SVGRectElement))
				break
			case SVGCircleElement:
				parent.children.push(Circle.fromDom(child as SVGCircleElement))
				break
			case SVGEllipseElement:
				parent.children.push(Ellipse.fromDom(child as SVGEllipseElement))
				break
			case SVGLineElement:
				parent.children.push(Line.fromDom(child as SVGLineElement))
				break
			case SVGPolygonElement:
			case SVGPolylineElement:
				parent.children.push(Poly.fromDom(child as SVGPolylineElement))
				break
			case SVGTextElement:
				parent.children.push(Text.fromDom(child as SVGTextElement))
				break
			case SVGTitleElement:
				if (parent instanceof Svg) parent.title = child.textContent ?? ""
				else console.warn("Title should not be in ", parent.name)
				break
			case SVGGradientElement:
			case SVGLinearGradientElement:
			case SVGRadialGradientElement:
				parent.children.push(Gradient.fromDom(child as SVGGradientElement))
				break
			case SVGStyleElement: {
				parent.children.push(new StyleSheetTag(child as SVGStyleElement))
				break
			}
			default:
				console.warn("not handle this dom element: ", child.constructor.name)
				if (child.children.length) parent.children.push(HasChildren.fromDom(child as SVGElement))
				else parent.children.push(Tag.fromDom(child))
		}
	}
	return parent
}
