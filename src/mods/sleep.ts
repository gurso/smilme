export function sleep(d = 0) {
	return new Promise(resolve =>
		setTimeout(() => {
			resolve(d)
		}, d),
	)
}
