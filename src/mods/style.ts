import { toTuple } from "./tuple"

export function parse(str?: string) {
	if (!str) return {}
	return Object.fromEntries(
		str
			.split(";")
			.filter(Boolean)
			.map(it => it.trim())
			.map(it =>
				toTuple(
					it.split(":").map(s => s.trim()),
					2,
				),
			),
	)
}

export function serialize(m: Record<string, string>) {
	return Object.entries(m)
		.map(([k, v]) => `${k}:${v}`)
		.join(";")
}
