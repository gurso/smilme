import { Text } from "../models/Text"

export function getTextSize(txt: Text) {
	const cv = document.getElementById("canvas")
	const canvas = document.createElement("canvas")
	const context = canvas.getContext("2d")
	if (!context || !cv) throw new Error("No context")
	const styles = getComputedStyle(cv)
	const fontSize = txt.attrs["font-size"] ?? styles.fontSize
	const fontFamily = txt.attrs["font-family"] ?? styles.fontFamily
	context.font = fontSize + " " + fontFamily
	context.textAlign = "center"
	context.fillText(txt.content, 0, 0)
	const metrics = context.measureText(txt.content)
	const { width, actualBoundingBoxAscent, actualBoundingBoxDescent } = metrics
	const height = actualBoundingBoxAscent + actualBoundingBoxDescent
	return { width, height }
}
