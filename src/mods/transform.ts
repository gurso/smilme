import { toTuple } from "./tuple"

const SEP = /\s*[ ,]\s*/

export type TransformValues = {
	translateY: number
	translateX: number
	rotate: number
	scale: number
}

export function parse(str: string) {
	const values = { translateX: 0, translateY: 0, scale: 1, rotate: 0 }
	for (const it of str.split(/(?<=\))/)) {
		const [prop, strValue] = it.replace(")", "").split("(")
		if (strValue?.trim()) {
			switch (prop) {
				case "translate":
					{
						const [x, y] = toTuple(
							strValue.split(SEP).map(s => Number(s.trim())),
							2,
						)
						values.translateX = x
						values.translateY = y
					}
					break
				case "scale":
					values[prop] = Number(strValue ?? 1)
					break
				case "rotate":
					values[prop] = Number(strValue ?? 0)
					break
			}
		}
	}
	return values
}

export function serialize(v: TransformValues) {
	return [
		v.translateX !== 0 || v.translateY !== 0 ? `translate(${v.translateX} ${v.translateY})` : "",
		v.scale !== 1 ? `scale(${v.scale})` : "",
		v.rotate && v.rotate !== 0 ? `rotate(${v.rotate})` : "",
	].join(" ")
}

export function parseOrigin(str: string) {
	const [x = 0, y = 0] = str.split(SEP).map(s => Number(s.trim()))
	return { x, y }
}

export function getRound(str: string, dec?: number) {
	const tr = parse(str)
	for (const k in tr) {
		const typedKey = k as keyof typeof tr
		tr[typedKey] = Number(tr[typedKey].toFixed(dec))
	}
	return serialize(tr)
}
