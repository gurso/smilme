import { ref } from "vue"
import { Svg } from "../models/Svg"

export const svg = ref(new Svg())

export const x = ref(0)
export const y = ref(0)
