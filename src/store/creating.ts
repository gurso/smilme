import { ref } from "vue"
import { Shape } from "../models"
import { HasChildren } from "../models/HasChildren"
import { add } from "../mods/coord"
import { useCursor } from "../composables/useCursor"
import { select } from "./selected"

export const parent = ref<HasChildren>()
export const creating = ref<Shape>()
export const cursorCSS = ref("default")

export function create(parent: HasChildren) {
	if (creating.value) {
		const { zoomed } = useCursor()
		creating.value.move(add(zoomed.value, creating.value.getCoord()))
		parent.children.push(creating.value)
		select(creating.value)
		creating.value = undefined
	}
}
