import { useStorage } from "@vueuse/core"
import { Svg } from "../models/Svg"
import { svg } from "./canvas"
import { parseSvgStr } from "../mods/file"
import { debounce } from "../mods/fn"
import { ref } from "vue"

// maxHistory and saved are number of character which ~= to bytes
export const maxHistory = ref(1_000_000)
const saved = ref(0)

export const history = useStorage<string[]>("history", [])
const i = ref(history.value.length - 1)

export function isSelected(j: number) {
	return i.value === j
}

export function load(index = i.value) {
	i.value = index
	svg.value = parseSvgStr(history.value[index] ?? new Svg().toString())
}

export function save() {
	const str = svg.value.toString()
	if (str !== history.value[i.value]) {
		history.value.splice(i.value + 1, history.value.length, str)
		saved.value += str.length
		while (saved.value > maxHistory.value) {
			const removed = history.value.shift()
			if (!removed) break
			saved.value -= removed.length
		}
		i.value = history.value.length - 1
	}
}

export const saveDebounced = debounce(save, 400)

export function undo() {
	i.value = Math.max(-1, i.value - 1)
	load()
}

export function redo() {
	i.value = Math.min(history.value.length - 1, i.value + 1)
	load()
}

export function clear() {
	history.value = []
	i.value = 0
	load()
}

export function rm(j: number) {
	const removed = history.value.splice(j, 1)
	saved.value -= removed.reduce((c, p) => c + p.length, 0)
}
