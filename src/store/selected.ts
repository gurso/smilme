import { ref, watch } from "vue"
import { IsCommand } from "../types/IsCommand"
import { Point } from "../models/Point"
import { Path } from "../models"
import { Poly } from "../models/Poly"
import { Tag } from "../models/Tag"

export const selected = ref<Tag[]>([])
export const command = ref<IsCommand>()
export const point = ref<Point>()
export const noding = ref<Path | Poly>()

watch(selected, () => {
	if (command.value) command.value = undefined
})

export function select(...shapes: Tag[]) {
	clean()
	add(...shapes)
}

export function unselect(...shapes: Tag[]) {
	selected.value = selected.value.filter(s => !shapes.includes(s))
}

export function selectCommand(target: IsCommand) {
	command.value = target
}
export function isSelectedCommand(target: IsCommand) {
	return target.id === command.value?.id
}

export function add(...shapes: Tag[]) {
	for (const shape of shapes) {
		if (!selected.value.includes(shape)) {
			selected.value.push(shape)
		}
	}
}

export function toggle(...shapes: Tag[]) {
	for (const shape of shapes) {
		if (selected.value.includes(shape)) {
			rm(shape)
		} else {
			add(shape)
		}
	}
}

export function rm(...shapes: Tag[]) {
	for (const shape of shapes) {
		const i = selected.value.indexOf(shape)
		if (i >= 0) {
			selected.value.splice(i, 1)
		}
	}
}

export function clean() {
	selected.value = []
}

export function isSelected(shape: Tag) {
	return selected.value.includes(shape)
}
