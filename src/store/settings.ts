import { useStorage } from "@vueuse/core"

export const grid = useStorage("settings-grid", {
	show: true,
	x: 10,
	y: 10,
})

export function reset() {
	grid.value = {
		show: true,
		x: 10,
		y: 10,
	}
}
