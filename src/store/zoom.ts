import { ref } from "vue"

export const zoom = ref(1)

export function zoomIn(step = 0.1) {
	zoom.value += step
}

export function zoomOut(step = 0.1) {
	zoom.value = Math.max(zoom.value - step, step)
}
