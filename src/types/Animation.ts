import { Animate } from "../models/Animate"
import { AnimateTransform } from "../models/AnimateTransform"
// import { AnimateMotion } from "../models/AnimateMotion"
// number ??
export type Timecount = `${number}` | `${number}ms` | `${number}s` | `${number}min` | `${number}h`
// export type ClockValue = ``

// clock-value : https://developer.mozilla.org/en-US/docs/Web/SVG/Content_type#clock-value
export type Value = string | number

export type AnimateAttrs = Partial<{
	dur: number
	repeatCount: number | "indefinite"
	repeatDur: number // ms to turn into clock-value
	restart: "always" | "whenNotActive" | "never"
	fill: "remove" | "freeze"

	// ignored if "values" is used
	from: Value
	by: Value
	to: Value
}>

export type Animation = Animate | AnimateTransform

export type TransformType = "rotate" | "translate" | "scale" | "skewX" | "skewY"
