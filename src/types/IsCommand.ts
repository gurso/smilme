import { CommandArc } from "../models/CommandArc"
import { CommandCubic } from "../models/CommandCubic"
import { CommandLine } from "../models/CommandLine"
import { CommandMove } from "../models/CommandMove"
import { CommandHorizontal } from "../models/CommandHorizontal"
import { CommandVertical } from "../models/CommandVertical"
import { CommandZ } from "../models/CommandZ"
import { CommandShortCubic } from "../models/CommandShortCubic"
import { CommandQuadratic } from "../models/CommandQuadratic"
import { CommandShortQuadratic } from "../models/CommandShortQuadratic"
import { Coord } from "./Shape"

type All =
	| CommandArc
	| CommandZ
	| CommandLine
	| CommandMove
	| CommandHorizontal
	| CommandVertical
	| CommandCubic
	| CommandShortCubic
	| CommandQuadratic
	| CommandShortQuadratic

export type IsCommand = All & Partial<Coord>
