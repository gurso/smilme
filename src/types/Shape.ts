export type Coord = {
	x: number
	y: number
}

export type RectBox = Coord & {
	width: number
	height: number
}
