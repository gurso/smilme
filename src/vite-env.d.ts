/// <reference types="vite/client" />
declare module "*.vue" {
	import { Component } from "vue"
	export default Component<>
}

interface String {
	toUpperCase<T extends string>(this: T): Uppercase<T>
	toLowerCase<T extends string>(this: T): Lowercase<T>
}
