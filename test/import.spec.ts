import { expect, test } from "vitest"
import { parseValues } from "../src/mods/commands"

test("Tests on parse method", () => {
	expect(parseValues("1 2 3 4")).toEqual([1, 2, 3, 4])
	expect(parseValues("11.23 2.34 35.9 4.32")).toEqual([11.23, 2.34, 35.9, 4.32])
	expect(parseValues("11.23 -2.34 -35.9 4.32")).toEqual([11.23, -2.34, -35.9, 4.32])
	expect(parseValues("11.23-2.34-35.9,4.32")).toEqual([11.23, -2.34, -35.9, 4.32])
	expect(parseValues("11.23-2.34-35.9,-.32")).toEqual([11.23, -2.34, -35.9, -0.32])
	expect(parseValues("11.23-2.34-35.9.32")).toEqual([11.23, -2.34, -35.9, 0.32])
})

test("wip", () => {
	expect(parseValues("5.23,5.92.93.27,2.53.53,4.39.84,1.07.18,2.24.37,3.45.59")).toEqual([
		5.23, 5.92, 0.93, 0.27, 2.53, 0.53, 4.39, 0.84, 1.07, 0.18, 2.24, 0.37, 3.45, 0.59,
	])
	expect(parseValues("614.92,153.65")).toEqual([614.92, 153.65])
})

test("test arc command parse", () => {
	expect(parseValues("40.4 40.4 0 01-4.2 2.7", true)).toEqual([40.4, 40.4, 0, 0, 1, -4.2, 2.7])
	expect(parseValues("1.4 1.4 0 00.6-.4", true)).toEqual([1.4, 1.4, 0, 0, 0, 0.6, -0.4])

	expect(parseValues("9.3 9.3 0 00-1.2.4 196.6 196.6 0 01-3.2 1.9", true)).toEqual([
		9.3, 9.3, 0, 0, 0, -1.2, 0.4, 196.6, 196.6, 0, 0, 1, -3.2, 1.9,
	])
})
