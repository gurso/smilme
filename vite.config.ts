import { defineConfig } from "vitest/config"
import vue from "@vitejs/plugin-vue"
import tailwindcss from "@tailwindcss/vite"

export default defineConfig(({ command }) => {
	return {
		base: command === "build" ? "/smilme/" : undefined,
		plugins: [vue(), tailwindcss()],
		test: {
			watch: false,
			environment: "jsdom",
		},
	}
})
